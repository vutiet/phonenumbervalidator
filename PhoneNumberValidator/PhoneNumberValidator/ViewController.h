//
//  ViewController.h
//  PhoneNumberValidator
//
//  Created by Vu Tiet on 6/20/14.
//  Copyright (c) 2014 Vu Tiet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate, UIPickerViewDelegate>

@end
