//
//  ViewController.m
//  PhoneNumberValidator
//
//  Created by Vu Tiet on 6/20/14.
//  Copyright (c) 2014 Vu Tiet. All rights reserved.
//

#import "ViewController.h"
#import "CountryPicker.h"
#import "NBPhoneNumber.h"
#import "NBNumberFormat.h"
#import "NBPhoneNumberUtil.h"
#import "NBAsYouTypeFormatter.h"

typedef enum {
    UnknownError = 0,
    ValidNumber,
    NoCountryCode,
    InvalidNumber,
    WrongCountryCode
} ValidationCode;

@interface ViewController ()

@property (nonatomic, retain) IBOutlet UITextField *countryCodeTextField;
@property (nonatomic, retain) IBOutlet UITextField *countryNameTextField;
@property (nonatomic, retain) IBOutlet UITextField *phoneNumberTextField;
@property (nonatomic, retain) IBOutlet UIPickerView *countryPicker;
@property (nonatomic, retain) IBOutlet UILabel *resultLabel;
@property (nonatomic, retain) NBAsYouTypeFormatter *phoneFormatter;

- (IBAction)validateButtonDidTouch:(id)sender;
- (ValidationCode)validatePhoneNumber:(NSString *)phoneNumber WithCountryCode:(NSString *)countryCode;
- (void)textField:(UITextField *)textField shouldHighlight:(BOOL)highlight;
- (NSString *)phoneNumberStringWithString:(NSString *)inputStr regionCode:(NSString *)regionCode;


@end

@implementation ViewController

@synthesize countryCodeTextField = _countryCodeTextField;
@synthesize countryNameTextField = _countryNameTextField;
@synthesize phoneNumberTextField = _phoneNumberTextField;
@synthesize countryPicker = _countryPicker;
@synthesize resultLabel = _resultLabel;
@synthesize phoneFormatter = _phoneFormatter;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Hides country picker
    self.countryPicker.hidden = YES;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Event Handling

- (IBAction)validateButtonDidTouch:(id)sender {
    // Hides all UI elements
    [self.phoneNumberTextField resignFirstResponder];
    self.countryPicker.hidden = YES;
    
    // Handles validation codes
    NSString *trimmedPhoneNumber = [self.phoneNumberTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    ValidationCode code = [self validatePhoneNumber:trimmedPhoneNumber WithCountryCode:self.countryCodeTextField.text];
    
    switch (code) {
        case NoCountryCode:
            self.resultLabel.text = @"Please select your country.";
            [self textField:self.countryCodeTextField shouldHighlight:YES];
            [self textField:self.countryNameTextField shouldHighlight:YES];
            [self textField:self.phoneNumberTextField shouldHighlight:NO];
            self.resultLabel.textColor = [UIColor redColor];
            break;
        case InvalidNumber:
            self.resultLabel.text = @"Please check your phone number.";
            [self textField:self.countryNameTextField shouldHighlight:NO];
            [self textField:self.countryCodeTextField shouldHighlight:NO];
            [self textField:self.phoneNumberTextField shouldHighlight:YES];
            self.resultLabel.textColor = [UIColor redColor];
            break;
        case ValidNumber:
            self.resultLabel.text = @"Your phone number is valid. Thank you!";
            [self textField:self.countryNameTextField shouldHighlight:NO];
            [self textField:self.countryCodeTextField shouldHighlight:NO];
            [self textField:self.phoneNumberTextField shouldHighlight:NO];
            self.resultLabel.textColor = [UIColor blueColor];
            break;
        case WrongCountryCode:
            self.resultLabel.text = @"Please select the country that matches your phone number!";
            [self textField:self.countryNameTextField shouldHighlight:YES];
            [self textField:self.countryCodeTextField shouldHighlight:YES];
            [self textField:self.phoneNumberTextField shouldHighlight:NO];
            self.resultLabel.textColor = [UIColor redColor];
            break;
        default:
            self.resultLabel.text = @"An unknown error happens, please try again!";
            break;
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // Hides country picker
    self.countryPicker.hidden = YES;
    
    // Clears result label
    self.resultLabel.text = @"";
    
    [self textField:self.phoneNumberTextField shouldHighlight:NO];

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // Hides keyboard and bliking cusor if it's country code or country name
    // Shows country picker instead
    if (textField == self.countryNameTextField || textField == self.countryCodeTextField) {
        
        [self textField:self.countryCodeTextField shouldHighlight:NO];
        [self textField:self.countryNameTextField shouldHighlight:NO];
        
        if (self.countryPicker.hidden) { // Only shows country picker if it's not showing
            [self.phoneNumberTextField resignFirstResponder]; // in case it's the first responder
            
            // Moves country picker to off-screen position then shows it
            self.countryPicker.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds), CGRectGetWidth(self.countryPicker.frame), CGRectGetHeight(self.countryPicker.frame));
            self.countryPicker.hidden = NO;
            
            // Move it to on-screen with animation
            [UIView animateWithDuration:0.5f animations:^{
                CGRect frame = self.view.bounds;
                frame.origin.y = CGRectGetHeight(frame) - CGRectGetHeight(self.countryPicker.frame);
                self.countryPicker.frame = frame;
            }];
        }
        self.resultLabel.text = @"";
        
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    if (textField == self.phoneNumberTextField) {
        
        // Doesn't allow to input non-numeric value if it's not the begining position
        if (range.location > 0) {
            if (string.length > 0 && !isnumber([string characterAtIndex:0])) {
                return NO;
            }
        }
        
        // Gets text before change
        NSMutableString *fullString = [NSMutableString stringWithString:textField.text];
        
        // Makes sure that always remove number
        if (string.length == 0 && range.location > 0 && !isnumber([fullString characterAtIndex:range.location])) {
            range.location = range.location - 1;
        }
        
        // Makes change
        [fullString replaceCharactersInRange:range withString:string];
        
        // Should put plug sign at the begining of phone number
        if ([fullString rangeOfString:@"+"].location != 0) {
            [fullString insertString:@"+" atIndex:0];
        }
        
        //    NSLog(@"phoneNumber %@", [self phoneNumberStringWithString:fullString regionCode:@"US"]);
        
        // Formats the string
        textField.text = [self phoneNumberStringWithString:fullString regionCode:self.countryCodeTextField.text];
        
        return NO;
    }
    return YES;
}

#pragma mark - CountryPickerDelegate

- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    // Sets values for country code & name
    self.countryCodeTextField.text = code;
    self.countryNameTextField.text = name;
    
    // Sets formatter
    NBAsYouTypeFormatter *formatter = [[NBAsYouTypeFormatter alloc] initWithRegionCode:self.countryCodeTextField.text];
    self.phoneFormatter = formatter;
}

#pragma mark - Private Methods

- (ValidationCode)validatePhoneNumber:(NSString *)phoneNumber WithCountryCode:(NSString *)countryCode {
   
    // Validates country code first
    if (!countryCode || countryCode.length == 0) {
        return NoCountryCode;
    }
    
    // Validates phone number
    NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil sharedInstance];
    NSError *error = nil;
    NBPhoneNumber *localePhoneNumber = [phoneUtil parse:phoneNumber defaultRegion:countryCode error:&error];
    if (error) {
        NSLog(@"err [%@]", [error localizedDescription]);
        return UnknownError;
    }
    
    
    if (![phoneUtil isValidNumber:localePhoneNumber]) {
        return InvalidNumber;
    }
    
    //    NSLog(@"- getRegionCodeForNumber [%@]", [phoneUtil getRegionCodeForNumber:localePhoneNumber]);
    if (![[phoneUtil getRegionCodeForNumber:localePhoneNumber] isEqualToString:countryCode]) {
        return WrongCountryCode;
    }


    return ValidNumber;
}

- (NSString *)phoneNumberStringWithString:(NSString *)inputStr regionCode:(NSString *)regionCode {
    NSString *result = @"";
    NSString *singleChar = @"";
    NSString *curStr = @"";
    NBAsYouTypeFormatter *formatter = [[NBAsYouTypeFormatter alloc] initWithRegionCode:regionCode];
    for (int i = 0; i < inputStr.length; i++) {
        
        if (isnumber([inputStr characterAtIndex:i]) || [[inputStr substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"+"]) {
            singleChar = [inputStr substringWithRange:NSMakeRange(i, 1)];
            curStr = [formatter inputDigit:singleChar];
            //            NSLog(@"singleChar %@ || curStr %@ || result %@", singleChar, curStr, result);
        }
    }
    result = curStr;
    return  result;
}

- (void)textField:(UITextField *)textField shouldHighlight:(BOOL)highlight {
    if (highlight) {
        textField.textColor = [UIColor redColor];
    }
    else {
        textField.textColor = [UIColor blackColor]; // default text color
    }
}


@end
